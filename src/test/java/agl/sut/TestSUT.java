package agl.sut;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

class TestSUT {


	@Test
	void testAjoutDe5éléments() throws TableauPleinException {
		SUT sut=new SUT(5);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		sut.ajout(4);
		sut.ajout(5);
		int[] tab=sut.values();
		int[] expected = {1,2,3,4,5};
		assertArrayEquals(expected,tab);
		assertEquals(5,sut.values().length);
	}

	@Test 
	void testExceptionQuandAjoutDansTableauPlein() throws TableauPleinException {
		SUT sut=new SUT(3);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		assertThrows(TableauPleinException.class, ()->{sut.ajout(4);});
	}
	
	@Test
	void testMinTableauRempliTrié() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(3);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		assertEquals(2,sut.values().length);
	}
	@Test
	void testMinTableauRempliNonTrié3() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(6);
		sut.ajout(4);
		sut.ajout(2);
		sut.ajout(1);
		sut.ajout(3);
		sut.ajout(5);
		sut.ajout(6);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		int a[] = {4,2,3,5,6};
		for (int element: sut.values()) 
			System.out.println(element);
		//assertEquals(a,sut.values());
		//assertArrayEquals(a,sut.values());
		
	}
	@Test
	void testMinTableauRempliNonTrié() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(3);
		sut.ajout(5);
		sut.ajout(1);
		sut.ajout(2);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		assertEquals(2,sut.values().length);
	}
	@Test
	void testMinTableauRempliNonTri2() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(4);
		sut.ajout(5);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(1);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertTrue(ArrayUtils.contains(sut.values(), 1));
		int a[] = {5,1,2};
		//assertEquals(sut.values(),a);
		//assertEquals(a,sut.values());
		assertArrayEquals(a,sut.values());
		//assertTrue(sut.values().equals(a));
	}
	
	@Test
	void testMinTableauVide() {
		SUT sut=new SUT(3);
		assertThrows(TableauVideException.class, ()->{sut.retourneEtSupprimePremiereOccurenceMin();});
	}
	
	
}
